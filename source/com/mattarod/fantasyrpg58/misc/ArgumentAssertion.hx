package com.mattarod.fantasyrpg58.misc;

/**
 * ...
 * @author Matthew Rodriguez
 */
class ArgumentAssertion {

	public static function checkOutOfBounds(index:Int, arrayLength:Int) {
		if (index < 0 || index >= arrayLength) {
			throw "Assert failure: index " + index + "out of bounds for array length " + arrayLength;
		}
	}
}
