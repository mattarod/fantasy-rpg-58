package com.mattarod.fantasyrpg58.states;

import com.mattarod.fantasyrpg58.scenario.ScenarioManager;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxSubState;
import flixel.text.FlxText;

/**
 * ...
 * @author Matthew Rodriguez
 */
class BattleState extends FlxSubState {
	var enemySprite:FlxSprite;
	var enemyName:FlxText;
	var enemyHPText:FlxText;
	var damageIndicator:FlxText;
	
	var heart:FlxSprite;
	var partyHPText:FlxText;
	var partyHPBarContainer:FlxSprite;
	
	var partyHPBar:FlxSprite;
	
	var player0Name:FlxText;
	var player0AttackButton:FlxText;
	
	var player1Name:FlxText;
	var player1AttackButton:FlxText;
	
	var player2Name:FlxText;
	var player2AttackButton:FlxText;
	
	var chargeBarContainer0:FlxSprite;
	var chargeBar0:FlxSprite;
	var readyLabel0:FlxText;
	
	var chargeBarContainer1:FlxSprite;
	var chargeBar1:FlxSprite;
	var readyLabel1:FlxText;
	
	var chargeBarContainer2:FlxSprite;
	var chargeBar2:FlxSprite;
	var readyLabel2:FlxText;

	var chargeTime0:Float = 0;
	var ready0:Bool;
	
	var chargeTime1:Float = 0;
	var ready1:Bool;
	
	var chargeTime2:Float = 0;
	var ready2:Bool;
	
	var enemyChargeTime:Float = 0;
	
	var hero:DungeonHero;
	var enemyHP:Int;
	var partyHP:Int;
	var victory:Bool;
	
	public static inline var CHARGE_BAR_SIZE:Int = 64;
	public static inline var BASE_SPEED = 16;
	public static inline var SPEED_FACTOR:Int = 2;
	
	public override function create():Void {
		ready0 = false;
		ready1 = false;
		ready2 = false;
		
		victory = false;
		enemyHP = ScenarioManager.enemyHP;
		partyHP = 100;
		hero = ScenarioManager.hero;
		
		FlxG.sound.play("Encounter");
		
		// Add backdrop
		var backdrop = new FlxSprite(0, 0, "png/battlebackdrop.png");
		add(backdrop);
		
		// Draw enemy sprite
		enemySprite = new FlxSprite(64, 8, "png/" + ScenarioManager.enemyImage + ".png");
		add(enemySprite);
		
		// Write enemy name
		enemyName = new FlxText(56, 48, 100, ScenarioManager.enemyName);
		enemyName.setFormat("fonts/nes.ttf", 8, 0x000000);
		add(enemyName);
		
		// Write enemy HP total
		enemyHPText = new FlxText(56, 56, 100, "HP: " + enemyHP);
		enemyHPText.setFormat("fonts/nes.ttf", 8, 0x000000);
		enemyHPText.visible = hero.hasLens;
		add(enemyHPText);
		
		// Draw party HP bar
		partyHPText = new FlxText(16, 76, 100, "" + partyHP);
		partyHPText.setFormat("fonts/nes.ttf", 8, 0x000000);
		add(partyHPText);
		
		heart = new FlxSprite(10, 78, "png/heart.png");
		add(heart);
		
		partyHPBarContainer = new FlxSprite(50, 76, "png/hpbar.png");
		add(partyHPBarContainer);
		
		partyHPBar = new FlxSprite(54, 77);
		partyHPBar.makeGraphic(100, 8);
		add(partyHPBar);
		
		// Write player name
		player0Name = new FlxText(8, 90, 72, "HERO");
		player0Name.setFormat("fonts/nes.ttf", 8, 0x000000);
		add(player0Name);
		
		player1Name = new FlxText(8, 104, 72, "WOMAN");
		player1Name.setFormat("fonts/nes.ttf", 8, 0x000000);
		add(player1Name);
		player1Name.exists = hero.partyMember1;
		
		player2Name = new FlxText(8, 118, 72, "RIVAL");
		player2Name.setFormat("fonts/nes.ttf", 8, 0x000000);
		add(player2Name);
		player2Name.exists = hero.partyMember2;
		
		// Write player attack button
		player0AttackButton = new FlxText(64, 90, 16, "X");
		player0AttackButton.setFormat("fonts/nes.ttf", 8, 0x000000);
		add(player0AttackButton);
		
		player1AttackButton = new FlxText(64, 104, 16, "C");
		player1AttackButton.setFormat("fonts/nes.ttf", 8, 0x000000);
		add(player1AttackButton);
		player1AttackButton.exists = hero.partyMember1;
		
		player2AttackButton = new FlxText(64, 118, 16, "V");
		player2AttackButton.setFormat("fonts/nes.ttf", 8, 0x000000);
		add(player2AttackButton);
		player2AttackButton.exists = hero.partyMember2;
		
		// Draw charge bar container
		chargeBarContainer0 = new FlxSprite(86, 90, "png/chargebar.png");
		add(chargeBarContainer0);
		
		chargeBarContainer1 = new FlxSprite(86, 104, "png/chargebar.png");
		add(chargeBarContainer1);
		chargeBarContainer1.exists = hero.partyMember1;
		
		chargeBarContainer2 = new FlxSprite(86, 118, "png/chargebar.png");
		add(chargeBarContainer2);
		chargeBarContainer2.exists = hero.partyMember2;
		
		// Initialize charge bars
		chargeBar0 = new FlxSprite(89, 91);
		chargeBar1 = new FlxSprite(89, 105);
		chargeBar2 = new FlxSprite(89, 119);
		
		chargeBar0.makeGraphic(1, 8);
		chargeBar1.makeGraphic(1, 8);
		chargeBar2.makeGraphic(1, 8);
		
		chargeBar1.exists = hero.partyMember1;
		chargeBar2.exists = hero.partyMember2;
		
		add(chargeBar0);
		add(chargeBar1);
		add(chargeBar2);
		
		// Ready label
		readyLabel0 = new FlxText(96, 90, 100, "READY!");
		readyLabel0.setFormat("fonts/nes.ttf");
		add(readyLabel0);
		readyLabel0.visible = false;
		
		readyLabel1 = new FlxText(96, 104, 100, "READY!");
		readyLabel1.setFormat("fonts/nes.ttf");
		add(readyLabel1);
		readyLabel1.exists = hero.partyMember1;
		readyLabel1.visible = false;
		
		readyLabel2 = new FlxText(96, 118, 100, "READY!");
		readyLabel2.setFormat("fonts/nes.ttf");
		add(readyLabel2);
		readyLabel2.exists = hero.partyMember2;
		readyLabel2.visible = false;
		
		// Create damage indicator
		damageIndicator = new FlxText(8, 132, 160);
		add(damageIndicator);
		damageIndicator.setFormat("fonts/nes.ttf");
	}
	
	public override function update():Void {
		if (victory) {
			this.close();
			return;
		}
		
		super.update();
		
		chargeTime0 += FlxG.elapsed * (BASE_SPEED + hero.getSpeed(0) * SPEED_FACTOR);
		chargeTime1 += FlxG.elapsed * (BASE_SPEED + hero.getSpeed(1) * SPEED_FACTOR);
		chargeTime2 += FlxG.elapsed * (BASE_SPEED + hero.getSpeed(2) * SPEED_FACTOR);
		enemyChargeTime += FlxG.elapsed * BASE_SPEED;
		
		// Draw HP Bar
		partyHPText.text = "" + partyHP;
		
		partyHPBar.makeGraphic(partyHP, 8, 0xff000000);
		
		// Hero charge
		if(!ready0) {
			if (chargeTime0 >= CHARGE_BAR_SIZE) {
				ready0 = true;
				FlxG.sound.play("Ready");
				readyLabel0.visible = true;
				
				chargeBar0.makeGraphic(1 + CHARGE_BAR_SIZE, 8, 0xff000000);
			} else {
				chargeBar0.makeGraphic(1 + Math.floor(chargeTime0), 8, 0xff000000);
			}
		}
		
		// Hero attack
		if (ready0) {
			if (FlxG.keys.justPressed.X) {
				enemyHP -= hero.getStrength(0);
				damageIndicator.text =  "HERO DOES " + hero.getStrength(0) + "DMG!";
				enemyHPText.text = "HP: " + enemyHP;
				
				if (enemyHP <= 0) {
					FlxG.sound.play("Death");
					victory = true;
					enemySprite.visible = false;
					this.openSubState(new VictoryState());
					return;
				}
				
				FlxG.sound.play("Attack");
				ready0 = false;
				chargeTime0 = 0;
				
				readyLabel0.visible = false;
			}
		}
		
		// Woman charge
		if(hero.partyMember1 && !ready1) {
			if (chargeTime1 >= CHARGE_BAR_SIZE) {
				ready1 = true;
				FlxG.sound.play("Ready");
				readyLabel1.visible = true;
				
				chargeBar1.makeGraphic(1 + CHARGE_BAR_SIZE, 8, 0xff000000);
			} else {
				chargeBar1.makeGraphic(1 + Math.floor(chargeTime1), 8, 0xff000000);
			}
		}
		
		// Woman attack
		if (hero.partyMember1 && ready1) {
			if (FlxG.keys.justPressed.C) {
				enemyHP -= (hero.getStrength(1));
				enemyHPText.text = "HP: " + enemyHP;
				damageIndicator.text =  "WOMAN DOES " + hero.getStrength(1) + "DMG!";
				
				if (enemyHP <= 0) {
					FlxG.sound.play("Death");
					victory = true;
					enemySprite.visible = false;
					this.openSubState(new VictoryState());
					return;
				}
				
				FlxG.sound.play("Attack");
				ready1 = false;
				chargeTime1 = 0;
				
				readyLabel1.visible = false;
			}
		}
		
		// Rival charge
		if(hero.partyMember2 && !ready2) {
			if (chargeTime2 >= CHARGE_BAR_SIZE) {
				ready2 = true;
				FlxG.sound.play("Ready");
				readyLabel2.visible = true;
				
				chargeBar2.makeGraphic(1 + CHARGE_BAR_SIZE, 8, 0xff000000);
			} else {
				chargeBar2.makeGraphic(1 + Math.floor(chargeTime2), 8, 0xff000000);
			}
		}
		
		// Rival attack
		if (hero.partyMember2 && ready2) {
			if (FlxG.keys.justPressed.V) {
				enemyHP -= (hero.getStrength(2));
				enemyHPText.text = "HP: " + enemyHP;
				damageIndicator.text = "RIVAL DOES " + hero.getStrength(2) + "DMG!";
				
				if (enemyHP <= 0) {
					FlxG.sound.play("Death");
					victory = true;
					enemySprite.visible = false;
					this.openSubState(new VictoryState());
					return;
				}
				
				FlxG.sound.play("Attack");
				ready2 = false;
				chargeTime2 = 0;
				
				readyLabel2.visible = false;
			}
		}
		
		// Enemy charge and attack
		if (enemyChargeTime >= CHARGE_BAR_SIZE) {
			partyHP -= 1;
			damageIndicator.text = ScenarioManager.enemyName + " ATTACKS!";
			FlxG.sound.play("EnemyAttack");
			enemyChargeTime = 0;
			
			if (partyHP <= 0) {
				FlxG.sound.play("GameOver");
				victory = false;
				this.close();
				ScenarioManager.dungeonState.openSubState(new GameOverState());
				return;
			}
		}
	}
}
