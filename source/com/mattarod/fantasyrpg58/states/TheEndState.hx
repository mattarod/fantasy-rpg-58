package com.mattarod.fantasyrpg58.states;

import flixel.FlxSprite;
import flixel.FlxSubState;
import flixel.text.FlxText;

/**
 * ...
 * @author Matthew Rodriguez
 */
class TheEndState extends FlxSubState {
	public var scriptPath:String;
	public var script:Array<String>;
	public var index:Int;
	public var text:FlxText;
	
	public override function create():Void {
		// Make backdrop
		var backdrop = new FlxSprite(0, 0, "png/cutscenebackdrop.png");
		add(backdrop);
		
		// Make text
		text = new FlxText(32, 64, 128, "THANK YOU\nFOR PLAYING!");
		text.setFormat("fonts/nes.ttf");
		add(text);
	}
}
