package com.mattarod.fantasyrpg58.states;

import com.mattarod.fantasyrpg58.scenario.ScenarioManager;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.system.scaleModes.PixelPerfectScaleMode;

class MenuState extends FlxState {
	public var scenarioManager:ScenarioManager;
	private static var setScaleMode:Bool = false;
	
	public override function create():Void {
		
		if (!setScaleMode)
		{
			FlxG.scaleMode = new PixelPerfectScaleMode();
			setScaleMode = true;
		}
		
		FlxG.camera.bgColor = 0xff000000;
		
		// Create the title screen text
		var title = new FlxSprite(0, 0, "png/title.png");
		add(title);
	}
	
	public override function destroy():Void {
		super.destroy();
	}

	public override function update():Void {
		if (FlxG.keys.justPressed.X) {
			FlxG.sound.play("OK");
			FlxG.switchState(new DungeonState());
		}
		
		super.update();
	}
}
