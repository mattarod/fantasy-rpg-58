package com.mattarod.fantasyrpg58.states;

import com.mattarod.fantasyrpg58.scenario.ScenarioManager;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxSubState;
import flixel.text.FlxText;
import openfl.Assets;

/**
 * ...
 * @author Matthew Rodriguez
 */
class TutorialState extends FlxSubState {
	public var scriptPath:String;
	public var script:String;
	public var text:FlxText;
	
	public override function create():Void {
		scriptPath = ScenarioManager.script;
		script = Assets.getText("scripts/" + scriptPath + ".txt");
		
		// Make backdrop
		var backdrop = new FlxSprite(8, 32, "png/tutorialbackdrop.png");
		add(backdrop);
		
		
		// Make text
		text = new FlxText(14, 38, 132, script);
		text.setFormat("fonts/nes.ttf");
		add(text);
		
		// Make prompt
		var prompt = new FlxText(120, 76, 32, "[X]");
		prompt.setFormat("fonts/nes.ttf");
		add(prompt);
	}
	
	public override function update():Void {
		if (FlxG.keys.justPressed.X) {
			FlxG.sound.play("OK");
			this.close();
			ScenarioManager.storyEvent.next();
		}
	}
}
