package com.mattarod.fantasyrpg58.states;

import com.mattarod.fantasyrpg58.scenario.ScenarioManager;
import com.mattarod.fantasyrpg58.scenario.StoryEvent;
import de.polygonal.ds.ListSet;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.group.FlxTypedGroup;
import flixel.tile.FlxTilemap;
import openfl.Assets;

/**
 * ...
 * @author Matthew Rodriguez
 */
class DungeonState extends FlxState {
	public var hero:DungeonHero;
	public var map:FlxTilemap;
	public var randomTrack:FlxSprite;
	public var solidEvents:FlxTypedGroup<FlxSprite>;
	
	public var eventGroup:FlxTypedGroup<StoryEvent>;
	
	public var roomX:Int;
	public var roomY:Int;
	
	public inline static var TILE_WIDTH = 16;
	public inline static var WALKING_SPEED = 60;
	public inline static var RANDOM_TRACK_SPEED = 10;
	
	override public function create():Void {
		// Set bg white
		FlxG.camera.bgColor = 0xffffffff;
		
		roomX = 0;
		roomY = 0;
		
		#if debug
		roomX = 4;
		roomY = -9;
		#end
		
		// Add hero
		hero = new DungeonHero();
		hero.x = 32;
		hero.y = 32;
		add(hero);
		
		// Add random battle counter.
		randomTrack = new FlxSprite();
		randomTrack.visible = false;
		add(randomTrack);
		
		// Initialize ScenarioManager.
		
		ScenarioManager.dungeonState = this;
		ScenarioManager.hero = hero;
		ScenarioManager.completedEventMap = new ListSet<Int>();
		
		solidEvents = new FlxTypedGroup<FlxSprite>();
		add(solidEvents);
		
		// Load the room.
		loadRoom();
	}
	
	public function loadRoom():Void {
		// Reset the random track.
		randomTrack.reset(0, 0);
		ScenarioManager.randomTrack = 0;
		
		// Unload the previous room.
		if (map != null) {
			remove(map);
			map.destroy();
			remove(eventGroup);
			eventGroup.destroy();
			remove(solidEvents);
			solidEvents.destroy();
		}
	
		
		// Load the tile map.
		map = new FlxTilemap();
		var mapFileName = "maps/map" + roomX + "," + roomY + ".txt";
		map.loadMap(Assets.getText(mapFileName), "png/maptiles.png", TILE_WIDTH, TILE_WIDTH, 1);
		add(map);
		
		//neko.Lib.println(prettyPrintMap(map.getData()));
		
		// Add events
		eventGroup = ScenarioManager.getRoomEvents(roomX, roomY);
		add(eventGroup);
		
		solidEvents = new FlxTypedGroup<FlxSprite>();
		add(solidEvents);
		
		for (event in eventGroup.members) {
			var dependents = event.getDependents();
			if (dependents != null) {
				for (sprite in dependents) {
					solidEvents.add(sprite);
				}
			}
		}
	}
	
	override public function update():Void {
		super.update();
		
		// Open pause menu
		if (FlxG.keys.justPressed.C) {
			var menu = new PauseState();
			openSubState(menu);
			return;
		}
		
		// Check for events
		FlxG.overlap(eventGroup, hero, doEvent);
		
		// Movement
		FlxG.collide(map, hero);
		FlxG.collide(solidEvents, hero);
		
		ScenarioManager.randomTrack = randomTrack.x;
		
		randomTrack.velocity.x = 0;
		
		if (FlxG.keys.pressed.UP) {
			hero.velocity.y = -WALKING_SPEED;
			randomTrack.velocity.x = RANDOM_TRACK_SPEED;
		} else if (FlxG.keys.pressed.DOWN) {
			hero.velocity.y = WALKING_SPEED;
			randomTrack.velocity.x = RANDOM_TRACK_SPEED;
		} else {
			hero.velocity.y = 0;
		}
		
		if (FlxG.keys.pressed.LEFT) {
			hero.velocity.x = -WALKING_SPEED;
			randomTrack.velocity.x = RANDOM_TRACK_SPEED;
		} else if (FlxG.keys.pressed.RIGHT) {
			hero.velocity.x = WALKING_SPEED;
			randomTrack.velocity.x = RANDOM_TRACK_SPEED;
		} else {
			hero.velocity.x = 0;
		}
		
		// Switch screens
		if (hero.x < -8) {
			roomX--;
			loadRoom();
			hero.reset(Constants.GAME_WIDTH-8, hero.y);
			
		} else if (hero.x > Constants.GAME_WIDTH-8) {
			roomX++;
			loadRoom();
			hero.reset(-8, hero.y);
			
		} else if (hero.y < -8) {
			roomY--;
			loadRoom();
			hero.reset(hero.x, Constants.GAME_HEIGHT-8);
			
		} else if (hero.y > Constants.GAME_HEIGHT-8) {
			roomY++;
			loadRoom();
			hero.reset(hero.x, -8);
		}
	}
	
	public function doEvent(a:FlxObject, b:FlxObject) {
		if (!Std.is(a, StoryEvent) || !Std.is(b, DungeonHero)) {
			throw "assert";
		}
		
		var event = cast(a, StoryEvent);
		
		if (event.begin(this)) {
			randomTrack.reset(0, 0);
			ScenarioManager.randomTrack = 0;
		}
	}
	
	public function prettyPrintMap(map:Array<Int>):String {
		var result:String = "";
		
		for (i in 0...9) {
			for (j in 0...10) {
				result += map[(i * 10) + j] + ",";
			}
			
			result += "\n";
		}
		
		return result;
	}
}
