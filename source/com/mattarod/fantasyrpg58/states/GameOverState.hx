package com.mattarod.fantasyrpg58.states;

import com.mattarod.fantasyrpg58.scenario.ScenarioManager;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxSubState;
import flixel.text.FlxText;

/**
 * ...
 * @author Matthew Rodriguez
 */
class GameOverState extends FlxSubState {
	public var scriptPath:String;
	public var script:Array<String>;
	public var index:Int;
	public var text:FlxText;
	public var text2:FlxText;
	
	public override function create():Void {
		// Make backdrop
		var backdrop = new FlxSprite(0, 0, "png/cutscenebackdrop.png");
		add(backdrop);
		
		// Make text
		text = new FlxText(44, 48, 128, "GAME OVER");
		text.setFormat("fonts/nes.ttf");
		add(text);
		
		text2 = new FlxText(16, 64, 144, "PRESS X TO RETRY");
		text2.setFormat("fonts/nes.ttf");
		add(text2);
	}
	
	public override function update():Void {
		if (FlxG.keys.justPressed.X) {
			FlxG.sound.play("OK");
			this.close();
			ScenarioManager.dungeonState.openSubState(new BattleState());
		}
	}
}
