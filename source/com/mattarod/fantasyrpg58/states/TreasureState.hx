package com.mattarod.fantasyrpg58.states;

import com.mattarod.fantasyrpg58.scenario.Items;
import com.mattarod.fantasyrpg58.scenario.ScenarioManager;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxSubState;
import flixel.text.FlxText;

/**
 * ...
 * @author Matthew Rodriguez
 */
class TreasureState extends FlxSubState {
	public var index:Int;
	public var script:Array<String>;
	public var text:FlxText;
	
	public override function create():Void {
		FlxG.sound.play("Treasure");
		
		var itemName:String = ScenarioManager.script;
		
		// Make backdrop
		var backdrop = new FlxSprite(8, 32, "png/tutorialbackdrop.png");
		add(backdrop);
		
		// Make prompt
		var prompt = new FlxText(120, 76, 32, "[X]");
		prompt.setFormat("fonts/nes.ttf");
		add(prompt);
		
		// Add item to inventory
		script = Items.get(itemName);
		index = 0;
		
		// Make text
		text = new FlxText(16, 40, 128, script[0]);
		text.setFormat("fonts/nes.ttf");
		add(text);
	}
	
	public override function update():Void {
		if (FlxG.keys.justPressed.X) {
			FlxG.sound.play("OK");
			index++;
			
			if (index >= script.length) {
				this.close();
				ScenarioManager.storyEvent.next();
			} else {
				text.text = script[index];
			}
		}
	}
}
