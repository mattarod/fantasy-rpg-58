package com.mattarod.fantasyrpg58.states;

import com.mattarod.fantasyrpg58.scenario.ScenarioManager;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxSubState;
import flixel.text.FlxText;

/**
 * ...
 * @author Matthew Rodriguez
 */
class VictoryState extends FlxSubState {
	public var text:FlxText;
	public var heroLeveledUp:Bool;
	public var levelUpMessageShown:Bool;
	public var hero:DungeonHero;
	
	public override function create():Void {
		// Make backdrop
		var backdrop = new FlxSprite(8, 32, "png/tutorialbackdrop.png");
		add(backdrop);
		
		hero = ScenarioManager.hero;
		var exp:Int = ScenarioManager.enemyHP;
		var totalExp = hero.exp + exp;
		var nextLevel = hero.expForNextLevel;
		
		// Make text
		text = new FlxText(16, 40, 128, "VICTORY!\n" +
			"EXP WON:   " + exp + "\n" +
			"EXP TOTAL: " + totalExp + "\n" +
			"NEXT LVL:  " + nextLevel
		);
		text.setFormat("fonts/nes.ttf");
		add(text);
		
		// Make prompt
		var prompt = new FlxText(120, 76, 32, "[X]");
		prompt.setFormat("fonts/nes.ttf");
		add(prompt);
		
		heroLeveledUp = hero.gainExp(exp);
		levelUpMessageShown = false;
	}
	
	public override function update():Void {
		if (FlxG.keys.justPressed.X) {
			if (heroLeveledUp && !levelUpMessageShown) {
				text.text = "LEVEL UP!\n" +
					"LEVEL:    " + hero.level + "\n" +
					"STRENGTH: " + hero.strength + "\n" +
					"SPEED:    " + hero.speed;
					
				FlxG.sound.play("LevelUp");
				
				levelUpMessageShown = true;
				
				return;
			}
			
			
			FlxG.sound.play("OK");
			this.close();
			ScenarioManager.dungeonState.closeSubState();
			
			if (ScenarioManager.storyEvent != null) {
				ScenarioManager.storyEvent.next();
			}
		}
	}
}
