package com.mattarod.fantasyrpg58.states;

import com.mattarod.fantasyrpg58.scenario.ScenarioManager;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxSubState;
import flixel.text.FlxText;
import openfl.Assets;

/**
 * ...
 * @author Matthew Rodriguez
 */
class CutSceneState extends FlxSubState {
	public var scriptPath:String;
	public var script:Array<String>;
	public var index:Int;
	public var text:FlxText;
	
	public override function create():Void {
		scriptPath = ScenarioManager.script;
		var plainScript:String = Assets.getText("scripts/" + scriptPath + ".txt");
		script = plainScript.split("\r\n");
		script.pop(); // Get rid of blank final line
		index = 0;
		
		// Make backdrop
		var backdrop = new FlxSprite(0, 0, "png/cutscenebackdrop.png");
		add(backdrop);
		
		// Make text
		text = new FlxText(16, 16, 128, script[0]);
		text.setFormat("fonts/nes.ttf");
		add(text);
		
		// Make prompt
		var prompt = new FlxText(120, 116, 32, "[X]");
		prompt.setFormat("fonts/nes.ttf");
		add(prompt);
	}
	
	public override function update():Void {
		if (FlxG.keys.justPressed.X) {
			FlxG.sound.play("OK");
			index++;
			
			if (index >= script.length) {
				this.close();
				ScenarioManager.storyEvent.next();
			} else {
				text.text = script[index];
			}
		}
	}
}
