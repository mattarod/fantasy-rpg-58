package com.mattarod.fantasyrpg58.states;

import com.mattarod.fantasyrpg58.scenario.ScenarioManager;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxSubState;
import flixel.text.FlxText;

/**
 * ...
 * @author Matthew Rodriguez
 */
class PauseState extends FlxSubState {
	public override function create():Void {
		FlxG.sound.play("OK");
		
		var hero:DungeonHero = ScenarioManager.hero;
		
		// Make backdrop
		var backdrop = new FlxSprite(0, 0, "png/pause.png");
		add(backdrop);
		
		// Render hero sprite
		var heroSprite0 = new FlxSprite(20, 20, "png/hero.png");
		add(heroSprite0);
		
		// Render hero name
		var name0 = new FlxText(40, 15, 128, "HERO");
		name0.setFormat("fonts/nes.ttf", 8, 0x000000);
		add(name0);
		
		// Render hero level
		var level0 = new FlxText(96, 15, 128, "LVL " + hero.level);
		level0.setFormat("fonts/nes.ttf", 8, 0x000000);
		add(level0);
		
		// Render hero strength
		var strength0 = new FlxText(40, 23, 128, "STR " + hero.getStrength(0));
		strength0.setFormat("fonts/nes.ttf", 8, 0x000000);
		add(strength0);
		
		// Render hero speed
		var speed0 = new FlxText(96, 23, 128, "SPD " + hero.getSpeed(0));
		speed0.setFormat("fonts/nes.ttf", 8, 0x000000);
		add(speed0);
		
		// Render weapon
		var weapon0 = new FlxText(40, 31, 128, hero.hasSword2 ? "*HOLY SWORD" : "*SHORT SWORD");
		weapon0.setFormat("fonts/nes.ttf", 8, 0x000000);
		add(weapon0);
		
		// Render second party member
		if (hero.partyMember1) {
			// Render hero sprite
			var heroSprite1 = new FlxSprite(20, 52, "png/woman.png");
			add(heroSprite1);
			
			// Render hero name
			var name1 = new FlxText(40, 47, 128, "WOMAN");
			name1.setFormat("fonts/nes.ttf", 8, 0x000000);
			add(name1);
			
			// Render hero level
			var level1 = new FlxText(96, 47, 128, "LVL " + hero.level);
			level1.setFormat("fonts/nes.ttf", 8, 0x000000);
			add(level1);
			
			// Render hero strength
			var strength1 = new FlxText(40, 55, 128, "STR " + hero.getStrength(1));
			strength1.setFormat("fonts/nes.ttf", 8, 0x000000);
			add(strength1);
			
			// Render hero speed
			var speed1 = new FlxText(96, 55, 128, "SPD " + hero.getSpeed(1));
			speed1.setFormat("fonts/nes.ttf", 8, 0x000000);
			add(speed1);
			
			// Render weapon
			var weapon1 = new FlxText(40, 63, 128, hero.hasDagger2 ? "*SHARP KNIFE" : "*KNIFE");
			weapon1.setFormat("fonts/nes.ttf", 8, 0x000000);
			add(weapon1);
		}
		
		// Render third party member
		if (hero.partyMember2) {
			// Render hero sprite
			var heroSprite2 = new FlxSprite(20, 84, "png/rival.png");
			add(heroSprite2);
			
			// Render hero name
			var name2 = new FlxText(40, 79, 128, "RIVAL");
			name2.setFormat("fonts/nes.ttf", 8, 0x000000);
			add(name2);
			
			// Render hero level
			var level2 = new FlxText(96, 79, 128, "LVL " + hero.level);
			level2.setFormat("fonts/nes.ttf", 8, 0x000000);
			add(level2);
			
			// Render hero strength
			var strength2 = new FlxText(40, 87, 128, "STR " + hero.getStrength(2));
			strength2.setFormat("fonts/nes.ttf", 8, 0x000000);
			add(strength2);
			
			// Render hero speed
			var speed2 = new FlxText(96, 87, 128, "SPD " + hero.getSpeed(2));
			speed2.setFormat("fonts/nes.ttf", 8, 0x000000);
			add(speed2);
			
			// Render weapon
			var weapon1 = new FlxText(40, 95, 128, hero.hasLance2 ? "*MAGIC STAFF" : "*STAFF");
			weapon1.setFormat("fonts/nes.ttf", 8, 0x000000);
			add(weapon1);
		}
		
		// Make prompt
		var prompt = new FlxText(120, 116, 32, "[X]");
		prompt.setFormat("fonts/nes.ttf", 8, 0x000000);
		add(prompt);
	}
	
	public override function update():Void {
		if (FlxG.keys.justPressed.X || FlxG.keys.justPressed.C) {
			FlxG.sound.play("Cancel");
			this.close();
		}
	}
}
