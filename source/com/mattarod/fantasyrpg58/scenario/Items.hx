package com.mattarod.fantasyrpg58.scenario;

/**
 * ...
 * @author Matthew Rodriguez
 */
class Items {
	public static function get(itemName):Array<String> {
		switch(itemName) {
			case "LENS":
				ScenarioManager.hero.hasLens = true;
				return ["THOU HAST FOUND THE LENS!", "NOW THOU CANST SEE THE ENEMY'S HP TOTAL IN BATTLE!"];
				
			case "PARTYMEMBER1":
				ScenarioManager.hero.partyMember1 = true;
				return ["THE YOUNG WOMAN HATH JOINED THY PARTY!", "WHEN HER ATTACK GAUGE IS FULL, PRESS C AND SHE WILL ATTACK."];
				
			case "PARTYMEMBER2":
				ScenarioManager.hero.partyMember2 = true;
				return ["THY RIVAL HATH JOINED THY PARTY!", "WHEN HIS ATTACK GAUGE IS FULL, PRESS V AND HE WILL ATTACK."];
				
			case "KNIFE2":
				ScenarioManager.hero.hasDagger2 = true;
				return ["THOU HAST FOUND THE SHARP KNIFE!", "THIS WEAPON INCREASETH THE YOUNG WOMAN'S ATTACK POWER BY 5."];
				
			case "SWORD2":
				ScenarioManager.hero.hasSword2 = true;
				return ["THOU HAST RECLAIMED THE HOLY SWORD!", "THIS WEAPON INCREASETH THY ATTACK POWER BY 10."];
				
			case "STAFF2":
				ScenarioManager.hero.hasLance2 = true;
				return ["THOU HAST FOUND THE MAGIC STAFF!", "THIS WEAPON INCREASETH THY RIVAL'S ATTACK POWER BY 20."];
				
			default:
				throw "No such item: " + itemName;
		}
	}
}
