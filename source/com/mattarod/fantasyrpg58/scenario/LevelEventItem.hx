package com.mattarod.fantasyrpg58.scenario;

import com.mattarod.fantasyrpg58.states.TutorialState;
import flixel.FlxState;

/**
 * ...
 * @author Matthew Rodriguez
 */
class LevelEventItem implements EventItem {
	public var level:Int;
	
	public function new(contents:String) {
		level = Std.parseInt(contents);
	}
	
	public function play(parentState:FlxState) {
		ScenarioManager.script = "script5";
		parentState.openSubState(new TutorialState());
		
		return ScenarioManager.hero.level >= level;
	}
}
