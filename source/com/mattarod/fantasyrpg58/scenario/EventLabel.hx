package com.mattarod.fantasyrpg58.scenario;

import de.polygonal.ds.Hashable;

/**
 * ...
 * @author Matthew Rodriguez
 */
class EventLabel implements Hashable {

	public var x:Int;
	public var y:Int;
	public var i:Int;
	
	public var key:Int;
	
	public function new(x1:Int, y1:Int, i1:Int) {
		x = x1;
		y = y1;
		i = i1;
	}
	
}
