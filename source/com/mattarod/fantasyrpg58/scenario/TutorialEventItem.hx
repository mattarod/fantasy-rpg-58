package com.mattarod.fantasyrpg58.scenario;

import com.mattarod.fantasyrpg58.states.TutorialState;
import flixel.FlxState;

/**
 * ...
 * @author Matthew Rodriguez
 */
class TutorialEventItem implements EventItem {
	public var message:String;
	
	public function new(scriptName:String) {
		message = scriptName;
	}
	
	public function play(parentState:FlxState) {
		ScenarioManager.script = message;
		parentState.openSubState(new TutorialState());
		return true;
	}
}
