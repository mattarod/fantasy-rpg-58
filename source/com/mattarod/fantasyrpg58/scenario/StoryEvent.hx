package com.mattarod.fantasyrpg58.scenario;

import com.mattarod.fantasyrpg58.states.DungeonState;
import flixel.FlxSprite;
import flixel.FlxState;

/**
 * ...
 * @author Matthew Rodriguez
 */
class StoryEvent extends FlxSprite {
	public var label:Int;
	public var items:Array<EventItem>;
	public var index:Int;
	public var parentState:FlxState;
	
	public function new(roomX:Int, roomY:Int, eventId:Int, eventItems:Array<EventItem>, x1:Int, y1:Int, w1:Int, h1:Int) {
		super();
		
		label = computeLabel(roomX, roomY, eventId);
		
		items = eventItems;
		x = x1 * DungeonState.TILE_WIDTH;
		y = y1 * DungeonState.TILE_WIDTH;
		width = w1 * DungeonState.TILE_WIDTH;
		height = h1 * DungeonState.TILE_WIDTH;
		
		index = 0;
		
		visible = false;
	}
	
	public function begin(state:FlxState):Bool {
		if (index == 0) {
			parentState = state;
			next();
		}
		
		return true;
	}
	
	public function next():Void {
		if (!alive) return;
		
		ScenarioManager.storyEvent = this;
		
		if (index >= items.length) {
			this.kill();
			return;
		}
		
		var item = items[index];
		
		var halt:Bool = !item.play(parentState);
		
		if (halt) {
			this.kill();
		} else {
			index++;
		}
	}
	
	public function computeLabel(x:Int, y:Int, i:Int):Int {
		return ((x + 128) * 256 * 256) + ((y + 128) * 256) + i;
	}
	
	public function getDependents():Array<FlxSprite> {
		return null;
	}
	
	public function neverPlayAgain() {
		ScenarioManager.storyEvent = null;
		ScenarioManager.completedEventMap.set(label);
		this.kill();
	}
}
