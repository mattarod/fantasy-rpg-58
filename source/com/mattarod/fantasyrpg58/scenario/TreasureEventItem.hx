package com.mattarod.fantasyrpg58.scenario;

import com.mattarod.fantasyrpg58.states.TreasureState;
import flixel.FlxState;

/**
 * ...
 * @author Matthew Rodriguez
 */
class TreasureEventItem implements EventItem {
	public var item:String;
	
	public function new(contents:String) {
		item = contents;
	}
	
	public function play(parentState:FlxState) {
		ScenarioManager.script = item;
		parentState.openSubState(new TreasureState());
		return true;
	}
}
