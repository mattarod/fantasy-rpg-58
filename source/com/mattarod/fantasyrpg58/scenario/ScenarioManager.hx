package com.mattarod.fantasyrpg58.scenario;

import com.mattarod.fantasyrpg58.misc.ArgumentAssertion;
import com.mattarod.fantasyrpg58.states.DungeonState;
import de.polygonal.ds.ListSet;
import flixel.group.FlxTypedGroup;
import openfl.Assets;

/**
 * ...
 * @author Matthew Rodriguez
 */
class ScenarioManager {
	public static var dungeonState:DungeonState;
	public static var storyEvent:StoryEvent;
	public static var script:String;
	
	public static var enemyName:String;
	public static var enemyImage:String;
	public static var enemyHP:Int;
	
	public static var hero:DungeonHero;
	
	public static var completedEventMap:ListSet<Int>;
	
	public static var randomTrack:Float;
	
	public static function getRoomEvents(x:Int, y:Int):FlxTypedGroup<StoryEvent> {
		var scenarioFileName:String = "scenario/map" + x + "," + y + ".txt";
		var scenarioData:String = Assets.getText(scenarioFileName);
		
		var scenarioDataAsLines:Array<String> = scenarioData.split("\r\n");
		
		var line:Int = 0;
		
		var storyEvents:FlxTypedGroup<StoryEvent> = new FlxTypedGroup<StoryEvent>();
		
		var eventIndex:Int = 0;
		
		while (line < scenarioDataAsLines.length  && scenarioDataAsLines[line] != "") {
			var eventType:String = scenarioDataAsLines[line];
				
			line++;
			ArgumentAssertion.checkOutOfBounds(line, scenarioDataAsLines.length);
			
			var positionalLine:String = scenarioDataAsLines[line];
			var positionArray:Array<String> = positionalLine.split(",");
			
			if (positionArray.length != 4) {
				throw "Invalid positional data: " + positionalLine;
			}
			
			line++;
			ArgumentAssertion.checkOutOfBounds(line, scenarioDataAsLines.length);
			var eventItemLine:String = scenarioDataAsLines[line];
			
			var eventItems:Array<EventItem> = new Array<EventItem>();
			
			// Extract event items
			while (line < scenarioDataAsLines.length && scenarioDataAsLines[line] != "") {
				
				switch(scenarioDataAsLines[line]) {
					case "script":
						line++;
						ArgumentAssertion.checkOutOfBounds(line, scenarioDataAsLines.length);
						var scriptName = scenarioDataAsLines[line];
						eventItems.push(new CutSceneEventItem(scriptName));
						
					case "tutorial":
						line++;
						ArgumentAssertion.checkOutOfBounds(line, scenarioDataAsLines.length);
						var scriptName = scenarioDataAsLines[line];
						eventItems.push(new TutorialEventItem(scriptName));
						
					case "battle":
						line++;
						ArgumentAssertion.checkOutOfBounds(line, scenarioDataAsLines.length);
						var enemyData:String = scenarioDataAsLines[line];
						var enemyDataArray:Array<String> = enemyData.split(",");
						
						if (enemyDataArray.length != 3) {
							throw "Invalid enemy data: " + enemyData;
						}
						
						var enemyName:String = enemyDataArray[0];
						var enemyImage:String = enemyDataArray[1];
						var enemyHP:Int = Std.parseInt(enemyDataArray[2]);
						
						eventItems.push(new BattleEventItem(enemyName, enemyImage, enemyHP));
						
					case "treasure":
						line++;
						ArgumentAssertion.checkOutOfBounds(line, scenarioDataAsLines.length);
						var contents = scenarioDataAsLines[line];
						eventItems.push(new TreasureEventItem(contents));
						
					case "level":
						line++;
						ArgumentAssertion.checkOutOfBounds(line, scenarioDataAsLines.length);
						var contents = scenarioDataAsLines[line];
						eventItems.push(new LevelEventItem(contents));
						
					case "destroy":
						eventItems.push(new DestroyEventItem());
						
					case "theend":
						eventItems.push(new TheEndEventItem());
						
					default:
						throw "EventItem type not supported: " + scenarioDataAsLines[line];
				}
				
				line++;
			}
			
			var coords:Array<Int> = new Array<Int>();
			
			for (s in positionArray) {
				coords.push(Std.parseInt(s));
			}
			
			switch(eventType) {
				case "invisible":
					var storyEvent = new StoryEvent(x, y, eventIndex, eventItems, coords[0], coords[1], coords[2], coords[3]);
					if(!completedEventMap.contains(storyEvent.label)) {
						storyEvents.add(storyEvent);
					}
					
				case "chest":
					var treasureEvent = new TreasureEvent(x, y, eventIndex, eventItems, coords[0], coords[1], coords[2], coords[3]);
					if(!completedEventMap.contains(treasureEvent.label)) {
						storyEvents.add(treasureEvent);
					}
					
				case "random":
					var randomEvent = new RandomEvent(x, y, eventIndex, eventItems, coords[0], coords[1], coords[2], coords[3]);
					if(!completedEventMap.contains(randomEvent.label)) {
						storyEvents.add(randomEvent);
					}
					
				case "block":
					var blockEvent = new BlockEvent(x, y, eventIndex, eventItems, coords[0], coords[1], coords[2], coords[3]);
					if(!completedEventMap.contains(blockEvent.label)) {
						storyEvents.add(blockEvent);
					}
					
				default:
					throw "Event type not supported: " + eventType;
			}
			
			line++;
			eventIndex++;
		}
		
		return storyEvents;
	}
}
