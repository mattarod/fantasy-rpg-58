package com.mattarod.fantasyrpg58.scenario;

import flixel.FlxState;

/**
 * ...
 * @author Matthew Rodriguez
 */
class DestroyEventItem implements EventItem {

	public function new() {
	
	}
	
	public function play(parentState:FlxState):Bool {
		ScenarioManager.storyEvent.neverPlayAgain();
		return true;
	}
}
