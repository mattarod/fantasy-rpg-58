package com.mattarod.fantasyrpg58.scenario;

import com.mattarod.fantasyrpg58.states.CutSceneState;
import flixel.FlxState;

/**
 * ...
 * @author Matthew Rodriguez
 */
class CutSceneEventItem implements EventItem {
	public var script:String;
	
	public function new(scriptName:String) {
		script = scriptName;
	}
	
	public function play(parentState:FlxState) {
		ScenarioManager.script = script;
		parentState.openSubState(new CutSceneState());
		return true;
	}
}
