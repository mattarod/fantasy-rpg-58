package com.mattarod.fantasyrpg58.scenario;

/**
 * ...
 * @author Matthew Rodriguez
 */
class TreasureEvent extends StoryEvent {

	public function new(roomX:Int, roomY:Int, eventId:Int, eventItems:Array<EventItem>, x1:Int, y1:Int, w1:Int, h1:Int) {
		super(roomX, roomY, eventId, eventItems, x1, y1, w1, h1);
		
		if(alive) {
			loadGraphic("png/chest.png");
			visible = true;
		}
	}
}
