package com.mattarod.fantasyrpg58.scenario;

import flixel.FlxState;

/**
 * ...
 * @author Matthew Rodriguez
 */
class RandomEvent extends StoryEvent {
	public static var numberOfSteps:Float;
	
	public static var MINIMUM_NUMBER_OF_STEPS = 10;
	public static var RANGE = 30;
	
	public override function new(roomX:Int, roomY:Int, eventId:Int, eventItems:Array<EventItem>, x1:Int, y1:Int, w1:Int, h1:Int) {
		super(roomX, roomY, eventId, eventItems, x1, y1, w1, h1);
		numberOfSteps = MINIMUM_NUMBER_OF_STEPS + Std.random(RANGE);
	}
	
	public override function begin(state:FlxState):Bool {
		if(ScenarioManager.randomTrack > numberOfSteps) {
			super.begin(state);
			return true;
		}
		
		return false;
	}
	
	public override function next() {
		ScenarioManager.storyEvent = this;
		
		if (index >= items.length) {
			ScenarioManager.storyEvent = null;
			numberOfSteps = MINIMUM_NUMBER_OF_STEPS + Std.random(RANGE);
			Std.random(RANGE);
			index = 0;
			return;
		}
		
		var item = items[index];
		
		index++;
		
		item.play(parentState);
	}
}
