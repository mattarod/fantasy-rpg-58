package com.mattarod.fantasyrpg58.scenario;

import com.mattarod.fantasyrpg58.states.DungeonState;
import flixel.FlxSprite;

/**
 * ...
 * @author Matthew Rodriguez
 */
class BlockEvent extends StoryEvent {
	var blocks:Array<FlxSprite>;
	
	public function new(roomX:Int, roomY:Int, eventId:Int, eventItems:Array<EventItem>, x1:Int, y1:Int, w1:Int, h1:Int) {
		super(roomX, roomY, eventId, eventItems, x1-1, y1-1, w1+2, h1+2);
		
		blocks = new Array<FlxSprite>();
		
		if(alive) {
			for (i in x1...(x1 + w1 + 1)) {
				for (j in y1...(y1 + h1 + 1)) {
					var block = new FlxSprite(i * DungeonState.TILE_WIDTH, j * DungeonState.TILE_WIDTH, "png/lock.png");
					block.immovable = true;
					blocks.push(block);
				}
			}
		}
	}
	
	public override function getDependents():Array<FlxSprite> {
		return blocks;
	}
	
	public override function neverPlayAgain() {
		for (block in blocks) {
			ScenarioManager.dungeonState.solidEvents.remove(block);
			block.destroy();
		}
		
		super.neverPlayAgain();
	}
}
