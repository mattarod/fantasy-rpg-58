package com.mattarod.fantasyrpg58.scenario;

import com.mattarod.fantasyrpg58.states.TheEndState;
import flixel.FlxState;

/**
 * ...
 * @author Matthew Rodriguez
 */
class TheEndEventItem implements EventItem {
	public function new() {
		
	}
	
	public function play(parentState:FlxState) {
		parentState.openSubState(new TheEndState());
		return false;
	}
}
