package com.mattarod.fantasyrpg58.scenario;

import com.mattarod.fantasyrpg58.states.BattleState;
import flixel.FlxState;

/**
 * ...
 * @author Matthew Rodriguez
 */
class BattleEventItem implements EventItem {

	public var name:String;
	public var image:String;
	public var hp:Int;
	
	public function new(enemyName:String, imagePath:String, enemyHP:Int) {
		name = enemyName;
		image = imagePath;
		hp = enemyHP;
	}
	
	public function play(parentState:FlxState) {
		ScenarioManager.enemyName = name;
		ScenarioManager.enemyImage = image;
		ScenarioManager.enemyHP = hp;
		parentState.openSubState(new BattleState());
		return true;
	}
}
