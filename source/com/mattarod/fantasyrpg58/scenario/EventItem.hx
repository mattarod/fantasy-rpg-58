package com.mattarod.fantasyrpg58.scenario;

import flixel.FlxState;

/**
 * ...
 * @author Matthew Rodriguez
 */
interface EventItem {
	public function play(parentState:FlxState):Bool;
}
