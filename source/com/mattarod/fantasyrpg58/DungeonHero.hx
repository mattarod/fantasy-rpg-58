package com.mattarod.fantasyrpg58;

import flixel.FlxSprite;

/**
 * ...
 * @author Matthew Rodriguez
 */
class DungeonHero extends FlxSprite {

	public var exp:Int;
	public var speed:Int;
	public var strength:Int;
	
	public var level:Int;
	public var expForNextLevel:Int;
	
	public var hasLens:Bool;
	public var hasSword2:Bool;
	public var hasDagger2:Bool;
	public var hasLance2:Bool;
	
	public var partyMember1:Bool;
	public var partyMember2:Bool;
	
	public static inline var P1_ATTACK:Float = .7;
	public static inline var P1_SPEED:Float = 1.5;
	
	public static inline var P2_ATTACK:Float = 1.8;
	public static inline var P2_SPEED:Float = 0.6;
	
	public static inline var EXP_BASE:Float = 1.6;
	
	public static inline var LEVEL_CAP = 30;
	
	public function new() {
		super();
		
		loadGraphic("png/hero.png", false, 16, 16);
		width = 12;
		height = 12;
		
		this.centerOffsets();
		
		//offset.x = 2;
		//offset.y = 2;
		
		level = 1;
		exp = 0;
		
		speed = 1;
		strength = 1;
		
		hasLens = false;
		hasSword2 = false;
		hasDagger2 = false;
		hasLance2 = false;
		
		partyMember1 = false;
		partyMember2 = false;
		
		#if debug
		speed = 30;
		strength = 30;
		level = 30;
		hasLens = true;
		partyMember1 = true;
		partyMember2 = true;
		hasDagger2 = true;
		hasSword2 = true;
		hasLance2 = true;
		#end
		
		expForNextLevel = Math.ceil(Math.pow(EXP_BASE, level+1));
	}
	
	public function gainExp(xp:Int):Bool {
		if (level == LEVEL_CAP) {
			return false;
		}
		
		var leveledUp:Bool = false;
		
		exp += xp;
		
		while (exp >= expForNextLevel) {
			level++;
			
			if (level == LEVEL_CAP) {
				expForNextLevel = 0;
			} else {
				expForNextLevel = Math.ceil(Math.pow(EXP_BASE, level + 1));
			}
			
			speed++;
			strength++;
			leveledUp = true;
		}
		
		return leveledUp;
	}
	
	public function getStrength(partyMember:Int) {
		switch(partyMember) {
			case 0:
				return strength  + (hasSword2 ? 10 : 0);
			case 1:
				return Math.ceil(strength * P1_ATTACK) + (hasDagger2 ? 5 : 0);
			case 2:
				return Math.ceil(strength * P2_ATTACK) + (hasLance2 ? 20 : 0);
			default:
				throw "No such party member " + partyMember;
				
		}
	}
	
	public function getSpeed(partyMember:Int) {
		switch(partyMember) {
			case 0:
				return speed;
			case 1:
				return Math.ceil(speed * P1_SPEED);
			case 2:
				return Math.ceil(speed * P2_SPEED);
			default:
				throw "No such party member " + partyMember;
				
		}
	}
}
